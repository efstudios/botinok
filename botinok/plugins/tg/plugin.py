from botinok.plugins import BasePlugin
from botinok.plugins.tg.web_handlers import TgWebHandler
from botinok.plugins.tg.bot import Bot

import asyncio
import traceback


def catch_exceptions(f):
    async def wrapper(self, chat, *args, **kwargs):
        try:
            return await f(self, chat, *args, **kwargs)
        except Exception as e:
            traceback.print_exc()
            await chat.send_text(str(e), parse_mode='Markdown')
            raise
    return wrapper


class Plugin(BasePlugin):
    bots: dict[str, Bot] = None
    commands: dict[str, str] = None
    webhook_url: str = None

    async def initialize(self, webhook_url, bots: dict[str, dict] = None):
        self.bots = {}
        self.commands = {}
        self.webhook_url = webhook_url

        for bot_id, config in bots.items():
            await self.add_bot(bot_id, config)

    def get_webhook_url(self, bot_id):
        return None if self.webhook_url is None else '/webhook/tg/{}'.format(bot_id)

    async def add_bot(self, bot_id, config):
        bot = Bot(self, bot_id, **config)
        bot.tg.add_command('^/help', self.__command_help)
        self.botinok.add_web_handler(self, self.get_webhook_url(bot_id), TgWebHandler, dict(bot_id=bot_id))
        await bot.update_webhook(None if self.webhook_url is None else self.webhook_url + self.get_webhook_url(bot_id))
        if self.webhook_url is None:
            asyncio.get_running_loop().create_task(bot.tg.loop())
        self.bots[bot_id] = bot
        return bot

    def bot(self, bot_id):
        return self.bots[bot_id]

    def add_command(self, regex, description, fn):
        for _, bot in self.bots.items():
            bot.tg.add_command(regex, catch_exceptions(fn))
        if description:
            self.commands[regex] = description

    def remove_command(self, regex):
        for _, bot in self.bots.items():
            for item in bot.tg._commands:
                if item[0] == regex:
                    bot.tg._commands.remove(item)
                    if regex in self.commands:
                        del self.commands[regex]
                    break

    async def __command_help(self, chat, expr):
        msg = ''
        for command, description in self.commands.items():
            msg += '{description}\n'.format(command=command, description=description)
        await chat.send_text(msg if msg else 'No commands defined', parse_mode='Markdown', disable_notification=True)
