#!/usr/bin/env python
import asyncio
import os
import logging
import tornado.httpclient
import botinok.settings as settings
from botinok import Botinok
 
logging.basicConfig(level=settings.LOG_LEVEL)
log = logging.getLogger('main')


async def get_botinok(**kwargs):
    bot = Botinok(**kwargs)
    await bot.initialize
    return bot


if __name__ == '__main__':
    io_loop = asyncio.get_event_loop()
    botinok = io_loop.run_until_complete(get_botinok(**settings.options))
    handlers = botinok.get_web_handlers()
    log.info(handlers)
    application = tornado.web.Application(handlers)
    port = os.environ.get('PORT') or settings.PORT
    application.listen(port)
    io_loop.run_forever()
