import re
from datetime import datetime, timedelta


def with_date(func):
    def wrapper(*args, **kwargs):
        if 'current_date' not in kwargs or kwargs['current_date'] is None:
            kwargs['current_date'] = datetime.now()
        return func(*args, **kwargs)
    return wrapper

# погода в (ижевске|пятницу)
# погода на банном


weekdays = {
    r'(понедельник|пн)': 0,
    r'(вторник|вт)': 1,
    r'(сред[у|а]{1}|ср)': 2,
    r'(четверг|чт)': 3,
    r'(пятниц[у|а]{1}|пт)': 4,
    r'(су[б]{1,2}от[у|а]{1}|сб)': 5,
    r'(воскресен[ь|и]{1}е|вс)': 6,
}


@with_date
def get_weekday(text: str, current_date=None):
    # погода на (пятницу)
    current_weekday = current_date.weekday()
    for patern, day in weekdays.items():
        result = re.search(r'\b{}\b'.format(patern), text, re.I | re.U)
        if result:
            if day < current_weekday:
                return current_date + timedelta(days=7 - current_weekday + day)
            else:
                return current_date + timedelta(days=day - current_weekday)


relative_days = {
    r'(сегодня)': 0,
    r'(завтра)': 1,
    r'(послезавтра)': 2,
    r'(послепослезавтра)': 3,
    r'(послепослепослезавтра)': 4,
    r'(послепослепослепослезавтра)': 5,
}


@with_date
def get_relative_date(text: str, current_date=None):
    # погода на (сегодня|завтра|послезавтра)
    for patern, day in relative_days.items():
        result = re.search(r'\b{}\b'.format(patern), text, re.I | re.U)
        if result:
            return current_date + timedelta(days=day)


calendar_ranges = {
    r'(день)': timedelta(days=1),
    r'(недел[я|ю]{1})': timedelta(days=7),
    r'(месяц)': timedelta(days=30),
}

calendar_ranges_suffixes = {
    r'(д[е]{0,1}н[ь|ей]{1,2})': timedelta(days=1),
    r'(недел[ь|я|ю]{1})': timedelta(days=7),
    r'(месяц[а|ев]{1,2})': timedelta(days=30),
}


def is_date_range(text: str):
    # погода на (неделю|месяц|два дня|три дня|пять дней)
    pass


@with_date
def get_date_unit(text: str, current_date=None):
    check_list = [get_relative_date, get_weekday]
    for func in check_list:
        res = func(text, current_date=current_date)
        if res is not None:
            return res


def parse_weather(text: str):
    res = re.search(
        r'погода[\s]+(в|на)[\s]+(.+)[\s]+(в|на)[\s]+(.+)', text, re.I | re.U)
    if res:
        date = get_date_unit(res[2])
        if date is None:
            location = res[2]
            date = get_date_unit(res[4])
        else:
            location = res[4]
        return date, location

    res = re.search(
        r'погода[\s]+(.+)[\s]+(в|на)[\s]+(.+)', text, re.I | re.U)
    if res:
        date = get_date_unit(res[1])
        if date is None:
            location = res[1]
            date = get_date_unit(res[3])
        else:
            location = res[3]
        return date, location

    res = re.search(r'погода[\s]+(в|на)[\s]+(.+)', text, re.I | re.U)
    if res:
        date = get_date_unit(res[2])
        if date is None:
            return datetime.now(), res[2]
        return date, None

    res = re.search(r'погода[\s]+(.+)', text, re.I | re.U)
    if res:
        date = get_date_unit(res[1])
        if date is None:
            return datetime.now(), res[1]
        return date, None

    res = re.search(r'\bпогода\b', text, re.I | re.U)
    if res:
        return datetime.now(), None


if __name__ == '__main__':
    print(parse_weather('погода'))
    print(parse_weather('погода на пятницу'))
    print(parse_weather('погода на завтра в ижевске'))
    print(parse_weather('погода в банном на сегодня'))
