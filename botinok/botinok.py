import logging
import asyncio
import aioredis

from botinok.cache import Cache

log = logging.getLogger('botinok')


class Botinok(object):
    redis = None
    cache = None

    def __init__(self, **kwargs):
        self.initialize = asyncio.Future()
        self.plugins = {}
        self.web_handlers = []
        asyncio.get_event_loop().create_task(self.__initialize_task(kwargs))

    async def __initialize_task(self, kwargs):
        try:
            await self.__initialize(**kwargs)
            self.initialize.set_result(True)
        except Exception as e:
            self.initialize.set_exception(e)

    async def __initialize(self, redis, enabled_plugins, **settings):
        # create redis connection pool
        self.redis = await aioredis.create_redis_pool(redis) if redis else None
        self.cache = Cache(self.redis)

        # import and initialize plugins
        for plugin_name in enabled_plugins:
            log.info('loading plugin {}...'.format(plugin_name))
            plugin_class = getattr(__import__('botinok.plugins.{}.plugin'.format(plugin_name), fromlist=['Plugin']), 'Plugin')

            self.plugins[plugin_name] = plugin_class(plugin_name, self)
            if 'plugins' in settings and plugin_name in settings['plugins']:
                await self.plugins[plugin_name].initialize(**settings['plugins'][plugin_name])
            else:
                await self.plugins[plugin_name].initialize()
            log.info('plugin {} initialized'.format(plugin_name))
        log.info('all enabled plugins successfully initialized')

    def add_web_handler(self, plugin, regexp, handler_class, kwargs):
        params = dict(plugin=plugin, botinok=self)
        if kwargs:
            params.update(kwargs)
        self.web_handlers.append((regexp, handler_class, params))

    def get_web_handlers(self):
        return self.web_handlers

    def get_plugin(self, name):
        if name in self.plugins:
            return self.plugins[name]
        raise Exception('Plugin {} not loaded'.format(name))
