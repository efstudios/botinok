import re
import json
import random
import aiohttp
import transliterate
from datetime import datetime, timedelta

from botinok.plugins import BasePlugin
from botinok.plugins.weather.regex import parse_weather


API_ROOT = 'http://api.openweathermap.org/data/2.5/'

# погода
# погода на (сегодня|завтра|пятницу|неделю|два дня)
# погода в (ижевске|пятницу)
# погода на банном


class Plugin(BasePlugin):
    vk = None

    async def initialize(self, key=None):
        self.key = key
        self.vk = self.botinok.get_plugin('vk')
        self.vk.add_command(r'\b(погода)\b', self.command_weather, None)

    def trans(self, text):
        try:
            return transliterate.translit(text, reversed=True)
        except transliterate.exceptions.LanguageDetectionError:
            return text

    async def request(self, action, **params):
        params['APPID'] = self.key
        async with aiohttp.ClientSession() as session:
            async with session.get('{root}/{action}'.format(root=API_ROOT, action=action), params=params) as resp:
                data = await resp.json()
                self.log.debug(data)
                return data

    async def request_current(self, location):
        return await self.request('weather', q=self.trans(location), units='metric')

    async def request_5days(self, location):
        return await self.request('forecast', q=self.trans(location), units='metric')

    def format(self, data, name=None):
        return '{name}\n{temp} градусов\nдавление {pressure}\nветер {wind_speed} м/с'.format(
            name=name,
            temp=data['main']['temp'],
            pressure=data['main']['pressure'] * 0.750062,
            wind_speed=data['wind']['speed'],
        )

    async def command_weather(self, group, expr, message):
        date, location = parse_weather(message['text'])
        if date is None and location is None:
            return
        if location is None:
            location = 'Ижевск'

        date_human = date.strftime('%d.%m.%Y')
        today = datetime.now()

        delta = date - today
        if delta <= timedelta(days=5):
            msg = '{date}\n'.format(
                location=location, date=date_human)
            data = await self.request_5days(location)
            if data['cod'] == '404':
                msg = 'Хочешь погоду в {location} на {date}?\nЯ не нашел такого места'.format(
                    location=location, date=date_human)
            else:
                msg += self.format(data['list'][delta.days],
                                   name=data['city']['name'])
        else:
            msg = 'Хочешь погоду в {location} на {date}?\nУ меня пока что нет таких данных'.format(
                location=location, date=date_human)

        await group.api.messages.send(chat_id=message['chat_id'], message=msg)
