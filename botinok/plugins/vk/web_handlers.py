import asyncio
import json
import logging

from botinok.plugins import BaseRequestHandler


class VkWebHandler(BaseRequestHandler):
    async def post(self):
        if self.request.body:
            data = json.loads(self.request.body.decode("utf-8"))
            group = self.plugin.group(data['group_id'])

            if data['type'] == 'confirmation':
                self.plugin.log.debug('vk confirmation request')
                return self.write(group.confirmation)

            self.write('ok')

            self.plugin.log.debug('vk webhook:')
            self.plugin.log.debug(self.request.body.decode("utf-8"))

            if data['type'] == 'message_new':
                asyncio.ensure_future(group.on_message(data['object']))

# user invited:
# {"type":"message_new","object":{"date":1558649521,"from_id":363172415,"id":0,"out":0,"peer_id":2000000001,"text":"","conversation_message_id":2,"action":{"type":"chat_invite_user","member_id":-181802480},"fwd_messages":[],"important":false,"random_id":0,"attachments":[],"is_hidden":false},"group_id":181802480}

# user deleted
# {"type":"message_new","object":{"date":1558649521,"from_id":363172415,"id":0,"out":0,"peer_id":2000000001,"text":"","conversation_message_id":2,"action":{"type":"chat_invite_user","member_id":-181802480},"fwd_messages":[],"important":false,"random_id":0,"attachments":[],"is_hidden":false},"group_id":181802480}

# message
# {"type":"message_new","object":{"date":1558650124,"from_id":363172415,"id":1,"out":0,"peer_id":363172415,"text":"test","conversation_message_id":1,"fwd_messages":[],"important":false,"random_id":0,"attachments":[],"is_hidden":false},"group_id":181802480}
