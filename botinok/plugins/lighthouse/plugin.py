import asyncio
import re

from botinok.plugins import BasePlugin


class Plugin(BasePlugin):
    tg = None
    listeners = None
    lock = asyncio.Lock()

    async def initialize(self):
        self.tg = self.botinok.get_plugin('tg')
        self.tg.add_command(r'^/lightoff$', '/lightoff Unsubscribe from all notifications', self.command_unsubscribe)
        self.tg.add_command(r'^/light[\s]+(.+)$', '`/light <regexp>` Subscribe to notifications', self.command_subscribe)
        self.tg.add_command(r'^(.+)$', None, self.command_listen)
        async with self.lock:
            self.listeners = await self.cache.get('listeners')
            if self.listeners is None:
                self.listeners = {}

    async def clear_listeners(self, bot_id, user_id):
        async with self.lock:
            if bot_id not in self.listeners:
                return
            for regex in self.listeners[bot_id]:
                self.listeners[bot_id][regex].remove(user_id)
                if not self.listeners[bot_id][regex]:
                    del self.listeners[bot_id][regex]
            await self.cache.set('listeners', self.listeners)

    async def add_listener(self, bot_id, regex, user_id):
        async with self.lock:
            if bot_id not in self.listeners:
                self.listeners[bot_id] = {}
            if regex not in self.listeners[bot_id]:
                self.listeners[bot_id][regex] = []

            self.listeners[bot_id][regex].append(user_id)
            await self.cache.set('listeners', self.listeners)

    async def command_subscribe(self, chat, expr):
        user_id = chat.message['from']['id']
        notify_regex = expr.group(1)
        await self.add_listener(chat.bot.api_token, notify_regex, user_id)
        await chat.reply(
            'Вы подписаны на `{}`'.format(notify_regex), parse_mode='Markdown'
        )

    async def command_unsubscribe(self, chat, expr):
        user_id = chat.message['from']['id']
        await self.clear_listeners(chat.bot, user_id)
        await chat.reply(
            'Все ваши подписки удалены', parse_mode='Markdown'
        )

    async def command_listen(self, chat, expr):
        bot_id = chat.bot.api_token

        if bot_id not in self.listeners:
            return
        for regex in self.listeners[bot_id]:
            m = re.search(regex, chat.message["text"], re.I)
            if m:
                for user_id in self.listeners[bot_id][regex]:
                    await self.light_user(user_id, chat, expr)

    @staticmethod
    async def light_user(user_id, chat, expr):
        await chat.bot.private(user_id).forward_message(chat.id, chat.message['message_id'])
