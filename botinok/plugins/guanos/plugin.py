import re
import json
import random
import aiohttp
import functools
from botinok.plugins import BasePlugin


def guano(name, guano_classes):
    def wrapper_decorator(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            kwargs['guano'] = name
            kwargs['guano_classes'] = guano_classes
            return func(self, *args, **kwargs)
        return wrapper
    return wrapper_decorator


class Plugin(BasePlugin):
    vk = None

    guanos = {
        'brakes': {
            r'\bформул[а|ы|е|у]{1}\b': 'формула',
            r'\bшиман[а|у|о]{1}\b': 'шимано',
            r'\bсрам[а|у]{0,1}\b': 'срам',
        },
        'bikes': {
            r'\bкок[е|и]{1}р\b': 'кокер',
            r'\bкон[а|ы|е|у]{1}\b': 'кона',
            r'\bш[у|y]т[е|e|и][р|p][а|ы|е|e|у|y]{0,1}\b': 'шутер',
            r'\b[к|г]{1}[о|а]{1}[м]{1,2}ен[ц|с]{1}аль\b': 'коменсаль',
            r'\b(лапьер[а|у|е|ы]{0,1}|lapierre)\b': 'лапьер',
            r'\b(пуш[и|е]р[а|у|е|ы]{0,1}|pusher)\b': 'пушер',
            r'\b([с|ш]тарк[а|у|е|и]{0,1}|stark)\b': 'старк',
            r'\b(норк[а|у|е|и|о]{1}|norco)\b': 'норко',
            r'\b(санта|кукуруз|santa)\b': 'сантакруз',
            r'\b(санта[\s]{0,1}круз|santa[\s]{0,1}cruz)\b': 'сантакруз',
        },
        'languages': {
            r'\b(п[и|е]тон[а|у]{0,1}|python)\b': 'питон',
            r'\b([д]{0,1}жав[а|у|е|ы]{1}|java)\b': 'джава',
            r'\b([д]{0,1}жа[б|в]{1}аскрипт[а|у|е|ы]{0,1}|js|javascript)\b': 'джаваскрипт',
        },
        'base': {
            r'\bреб[а|у]{1}\b': 'реба',
            r'\bфр[и|е]ройд\b': 'фреройд',
        }
    }

    classes = {
        'base': ['говно', 'пиздец', 'хуйня', 'уебанство', 'ниочень', 'пережитокпрошлого', 'ахтунг', 'хуита', 'провал', 'дляизвращенцев', 'ниработаит'],
        'brakes': ['течет', 'греется', 'нетормозят'],
        'bikes': ['трещит', 'лопается', 'рвется', 'чугуний'],
        'languages': ['неязык', 'тормозит', 'длянубов'],
    }

    async def initialize(self, groups=None):
        self.vk = self.botinok.get_plugin('vk')

        for guano_class, guanos in self.guanos.items():
            for regex, name in guanos.items():
                guano_classes = [] + self.classes['base'] + self.classes[guano_class]
                self.vk.add_command(regex, guano(name, guano_classes)(
                    self.command_guano_detected), None
                )

    async def command_guano_detected(self, group, expr, message, guano=None, guano_classes=None):
        text = '#{guano}{alias}'.format(
            guano=guano, alias=random.choice(guano_classes)
        )
        await group.api.messages.send(chat_id=message['chat_id'], message=text)
