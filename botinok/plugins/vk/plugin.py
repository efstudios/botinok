from botinok.plugins import BasePlugin
from botinok.plugins.vk.web_handlers import VkWebHandler
from aiovk.sessions import TokenSession
from aiovk.api import API
from botinok.cache import Cache
import re


class Plugin(BasePlugin):
    groups = None

    async def initialize(self, groups=None):
        self.groups = {}
        for group_id, group in groups.items():
            self.groups[group_id] = Group(self, group_id, **group)

        self.botinok.add_web_handler(self, '/webhook/vk', VkWebHandler, None)

    def group(self, group_id):
        return self.groups[group_id]

    def add_command(self, *args, **kwargs):
        for _, group in self.groups.items():
            group.add_command(*args, **kwargs)


class CommandsMixin(object):
    _commands = None

    def __init__(self):
        self._commands = []

    def add_command(self, regexp, handler, description=None):
        self._commands.append((regexp, handler, description))

    async def on_message(self, message):
        message['chat_id'] = message['peer_id'] - \
            2000000000 if message['peer_id'] > 2000000000 else None

        m = re.search(r'\[[\w\d]+\|@katogram_bot\] (.+)',
                      message['text'], re.I)
        if m:
            message['text'] = m[1]

        for patterns, handler, _ in self._commands:
            m = re.search(patterns, message['text'],
                          re.IGNORECASE | re.UNICODE)
            if m:
                return await handler(self, m, message)


class Group(CommandsMixin):
    def __init__(self, plugin, group_id, access_token, confirmation):
        super(Group, self).__init__()
        self.plugin = plugin
        self.group_id = group_id
        self.confirmation = confirmation
        self.api = API(TokenSession(access_token=access_token))
        self.cache = Cache(plugin.botinok.redis, 'plugin:vk:group:{}'.format(
            self.group_id) + ':{key}')

    async def get_user(self, user_id):
        user = await self.cache.get('user:{}'.format(user_id))
        if user:
            return user

        user = (await self.api.users.get(user_ids=user_id, fields='photo_50,city,verified'))[0]
        await self.cache.set('user:{}'.format(user_id), user, expires=86400)
        return user
