import tornado.web
import logging
from abc import ABC
from botinok.cache import Cache


class BasePlugin(object):
    name = None
    botinok = None

    def __init__(self, name, botinok):
        self.name = name
        self.botinok = botinok
        self.log = logging.getLogger('botinok:{}'.format(self.name))
        self.cache = Cache(self.botinok.redis,
                           'plugin:{}'.format(self.name) + ':{key}')

    async def initialize(self, **options):
        pass

    def get_web_handlers(self):
        pass


class BaseRequestHandler(tornado.web.RequestHandler, ABC):
    plugin = None
    botinok = None

    def initialize(self, plugin=None, botinok=None, **kwargs):
        self.plugin = plugin
        self.botinok = botinok
