from botinok.cache import Cache

import aiotg


class Bot(object):
    def __init__(self, plugin, bot_id, access_token):
        super(Bot, self).__init__()
        self.plugin = plugin
        self.bot_id = bot_id
        self.tg = aiotg.Bot(api_token=access_token)
        self.cache = Cache(plugin.botinok.redis, 'plugin:tg:bot:{}'.format(self.bot_id) + ':{key}')

    async def update_webhook(self, webhook_url):
        self.plugin.log.debug('webhook url: ' + str(webhook_url))
        if webhook_url:
            await self.tg.set_webhook(webhook_url)
        else:
            await self.tg.delete_webhook()
