import re
import json
import random
import aiohttp
from botinok.plugins import BasePlugin


class Plugin(BasePlugin):
    vk = None

    phrases = [
        'Первый раз слышу.',
        'А мне никто не говорил.',
        'Мне не передавали.',
        'Я попробовал, но не получилось.',
        'Я ему сказал, а он не сделал.',
        'Туда не возможно дозвониться.',
        'Я поискал, но не нашел.',
        'Меня в это время не было, я болел.',
        'Это невозможно.',
        'Это бред.',
        'У нас обед.',
        'Рабочий день закончился.',
        'Мне не объяснили.',
        'Я как раз собирался этим заняться.',
        'Это было ещё до меня.',
        'А что поделать.',
        'Я этим не занимаюсь.',
        'Я не для этого нанимался.',
        'Это не мои проблемы.',
        'Спасибо в карман не положишь.',
        'Спасибо не булькает.',
        'А я ему постоянно говорил.',
        'Мы всё сделали, как написано в инструкции.',
        'Хотел как лучше, а получилось как всегда.',
        'Ну вот опять.',
        'Это всё Серёга.',
    ]

    async def initialize(self, groups=None):
        self.vk = self.botinok.get_plugin('vk')
        self.vk.add_command(r'\b(бот)\b', self.command_talk_ru, None)
        self.vk.add_command(r'\b(bot)\b', self.command_talk, None)

    @classmethod
    def get_random_phrase(cls):
        return random.choice(cls.phrases)

    @staticmethod
    async def get_fucking_great_advice():
        async with aiohttp.ClientSession() as session:
            async with session.get('http://fucking-great-advice.ru/api/random') as r:
                data = json.loads((await r.content.read()).decode('utf8'))
                return data['text']

    @staticmethod
    async def get_devanswers():
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/63.0.3239.84 Safari/537.36'
        }
        async with aiohttp.ClientSession() as session:
            async with session.get('http://devanswers.ru/', headers=headers) as r:
                content = (await r.content.read()).decode('utf8')
                prog = re.compile(r'initial = {"text":"(.*)","link"', re.UNICODE | re.MULTILINE | re.IGNORECASE)
                for s in prog.findall(content):
                    return s.encode('utf8').decode('unicode-escape').replace(r'<br\/>', '\n')

    async def get_forismatic(self, key, lang='en'):
        result = None
        num_of_times = 0
        while result is None and num_of_times < 10:
            num_of_times += 1
            async with aiohttp.ClientSession() as session:
                async with session.get('https://api.forismatic.com/api/1.0/',
                                       params={
                                           'method': 'getQuote',
                                           'key': key,
                                           'format': 'json',
                                           'lang': lang,
                                       }) as r:
                    data = json.loads((await r.content.read()).decode('utf8'))
                    result = data['quoteText']
        return result

    async def command_talk_ru(self, group, expr, message):
        user = await group.get_user(message['from_id'])
        
        rand = random.randint(1, 6)
        if rand == 1:
            phrase = await self.get_fucking_great_advice()
        elif rand == 2:
            phrase = await self.get_forismatic(group.group_id, lang='ru')
        elif rand < 5:
            phrase = await self.get_devanswers()
        else:
            phrase = self.get_random_phrase()

        text = '{name}, {phrase}'.format(name=user['first_name'], phrase=phrase)
        await group.api.messages.send(chat_id=message['chat_id'], message=text)

    async def command_talk(self, group, expr, message):
        user = await group.get_user(message['from_id'])

        text = '{name}, {phrase}'.format(name=user['first_name'], phrase=await self.get_forismatic(group.group_id))
        await group.api.messages.send(chat_id=message['chat_id'], message=text)
