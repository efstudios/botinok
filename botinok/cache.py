import json
import logging

log = logging.getLogger('botinok.cache')


class Cache(object):
    __REDIS_PREFIX = 'botinok:{key}'
    _STORAGE_PREFIX = '{key}'

    cache = None

    def __init__(self, redis, prefix='general:{key}'):
        self._STORAGE_PREFIX = prefix
        self.redis = redis
        self.cache = {}

    def key(self, key):
        return self.__REDIS_PREFIX.format(key=self._STORAGE_PREFIX).format(key=key)

    async def set(self, key, value, expires=None):
        key = self.key(key)
        value_json = json.dumps(value)
        log.debug('cache: set %s to %s' % (key, value_json))
        if self.redis is None:
            self.cache[key] = value_json
        else:
            await self.redis.set(key, value_json, expire=expires)

    async def get(self, key):
        key = self.key(key)
        if self.redis is None:
            value_json = self.cache[key] if key in self.cache else None
        else:
            value_json = await self.redis.get(key)
        log.debug('cache: get %s: %s' % (key, value_json))
        return json.loads(value_json.decode('utf-8')) if value_json is not None else value_json

    async def clean(self, key):
        log.debug('cache: clear %s' % (key,))
        if self.redis is None:
            if key is None:
                del self.cache
            else:
                del self.cache[key]
        else:
            if not key:
                key = '*'
            key = self.key(key)
            await self.redis.delete(key)
