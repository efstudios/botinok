import json
import logging

from botinok.plugins import BaseRequestHandler

log = logging.getLogger('web')


class TgWebHandler(BaseRequestHandler):
    bot = None

    def initialize(self, plugin=None, botinok=None, **kwargs):
        super(TgWebHandler, self).initialize(plugin=plugin, botinok=botinok)
        self.bot = kwargs.pop('bot')

    async def post(self, bot_id):
        if self.request.body:
            data = self.request.body.decode("utf-8")
            self.plugin.log.debug('vk webhook:')
            self.plugin.log.debug(data)
            data = json.loads(data)
            self.bot.tg._process_update(data)
